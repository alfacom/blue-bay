//HTML ENCODE/DECODE + RUS TO CP1251 TODO: OVERRIDE html_encode after fix
/proc/rhtml_encode(var/msg)
	msg = list2text(text2list(msg, "<"), "&lt;")
	msg = list2text(text2list(msg, ">"), "&gt;")
	msg = list2text(text2list(msg, "�"), "&#255;")
	return msg

/proc/rhtml_decode(var/msg)
	msg = list2text(text2list(msg, "&gt;"), ">")
	msg = list2text(text2list(msg, "&lt;"), "<")
	msg = list2text(text2list(msg, "&#255;"), "�")
	return msg


//UPPER/LOWER TEXT + RUS TO CP1251 TODO: OVERRIDE uppertext
/proc/rupperext(text as text)
	text = uppertext(text)
	var/t = ""
	for(var/i = 1, i <= length(text), i++)
		var/a = text2ascii(text, i)
		if (a > 223)
			t += ascii2text(a - 32)
		else if (a == 184)
			t += ascii2text(168)
		else t += ascii2text(a)
	t = replacetext(t,"&#255;","�")
	return t

/proc/rlowertext(text as text)
	text = lowertext(text)
	var/t = ""
	for(var/i = 1, i <= length(text), i++)
		var/a = text2ascii(text, i)
		if (a > 191 && a < 224)
			t += ascii2text(a + 32)
		else if (a == 168)
			t += ascii2text(184)
		else t += ascii2text(a)
	return t


//TEXT SANITIZATION + RUS TO CP1251
sanitize_simple(var/t,var/list/repl_chars = list("\n"="#","\t"="#","�"="&#255;","<"="(",">"=")"))
	for(var/char in repl_chars)
		var/index = findtext(t, char)
		while(index)
			t = copytext(t, 1, index) + repl_chars[char] + copytext(t, index+1)
			index = findtext(t, char)
	return t



//RUS CONVERTERS
/proc/russian_to_cp1251(var/msg)//CHATBOX
	return list2text(text2list(msg, "�"), "&#255;")

/proc/russian_to_utf8(var/msg)//PDA PAPER POPUPS
	return list2text(text2list(msg, "�"), "&#1103;")

/proc/utf8_to_cp1251(msg)
    return list2text(text2list(msg, "&#1103;"), "&#255;")

/proc/cp1251_to_utf8(msg)
    return list2text(text2list(msg, "&#255;"), "&#1103;")



//TEXT MODS RUS
/proc/capitalize_cp1251(var/t as text)
	var/s = 2
	if (copytext(t,1,2) == ";")
		s += 1
	else if (copytext(t,1,2) == ":")
		s += 2
	return rupperext(copytext(t, 1, s)) + copytext(t, s)

/proc/intonation(text)
	if (copytext(text,-1) == "!")
		text = "<b>[text]</b>"
	return text



//SLURR/STUTTER USING RUSSIAN CHARS
slur(phrase)
	phrase = rhtml_decode(phrase)
	var/leng=lentext(phrase)
	var/counter=lentext(phrase)
	var/newphrase=""
	var/newletter=""
	while(counter>=1)
		newletter=copytext(phrase,(leng-counter)+1,(leng-counter)+2)
		if(rand(1,3)==3)
			if(lowertext(newletter)=="�")	newletter="�"
			if(lowertext(newletter)=="�")	newletter="�"
			if(lowertext(newletter)=="�")	newletter="�"
			if(lowertext(newletter)=="�")	newletter="�"
			if(lowertext(newletter)=="�")	newletter="�"
		switch(rand(1,15))
			if(1,3,5,8)	newletter="[lowertext(newletter)]"
			if(2,4,6,15)	newletter="[uppertext(newletter)]"
			if(7)	newletter+="'"
			//if(9,10)	newletter="<b>[newletter]</b>"
			//if(11,12)	newletter="<big>[newletter]</big>"
			//if(13)	newletter="<small>[newletter]</small>"
		newphrase+="[newletter]";counter-=1
	return newphrase

stutter(n)
	var/te = rhtml_decode(n)
	var/t = ""//placed before the message. Not really sure what it's for.
	n = length(n)//length of the entire word
	var/p = null
	p = 1//1 is the start of any word
	while(p <= n)//while P, which starts at 1 is less or equal to N which is the length.
		var/n_letter = copytext(te, p, p + 1)//copies text from a certain distance. In this case, only one letter at a time.
		if (prob(80) && (n_letter in list("�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�")))
			if (prob(10))
				n_letter = text("[n_letter]-[n_letter]-[n_letter]-[n_letter]")//replaces the current letter with this instead.
			else
				if (prob(20))
					n_letter = text("[n_letter]-[n_letter]-[n_letter]")
				else
					if (prob(5))
						n_letter = null
					else
						n_letter = text("[n_letter]-[n_letter]")
		t = text("[t][n_letter]")//since the above is ran through for each letter, the text just adds up back to the original word.
		p++//for each letter p is increased to find where the next letter will be.
	return sanitize(copytext(t,1,MAX_MESSAGE_LEN))